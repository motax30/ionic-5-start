import { Injectable } from '@angular/core';
import { CardModel } from '../model/card-model';

@Injectable({
  providedIn: 'root'
})
export class MovieService {
  movieKey = "movies";
  movies: Array<CardModel> = [];

  constructor() {
    // Recupera o json em string do localstorage
    const storedMovies = localStorage.getItem(this.movieKey);
    if (storedMovies) 
      this.movies = JSON.parse(storedMovies); // Transforma o json de string para objeto
  }

  // Promisses são usadas quando precisamos fazer algum processamento que pode demorar e não temos como saber quanto tempo
  // Para não travar o app, nós usamos promisses que tem um metodo chamado "then()" que é executado quando obtivermos uma resposta.
  getMovies(): Promise<Array<CardModel>> {
    return new Promise((resolve, reject) => {
      resolve(this.movies); // retorna os filmes
    });
  }

  addMovie(movie: CardModel) {
    movie.id = this.movies.length + 1;
    this.movies.push(movie);
    
    // Salvo o array novamente no localstorage após adicionar o filme
    localStorage.setItem(this.movieKey, JSON.stringify(this.movies));
  }

  getMovie(id: number): Promise<CardModel> {
    return new Promise((resolve, reject) => {
      const movie = this
        .movies
        .find(movie => movie.id == id);
      resolve(movie);
    });
  }
}
