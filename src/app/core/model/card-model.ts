export interface CardModel {
    id: number;
    title: string;
    subtitle: string;
    content: string;
    image: string;
}
