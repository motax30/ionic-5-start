import { Component, OnInit } from '@angular/core';
import { CardModel } from '../../core/model/card-model';
import { MovieService } from '../../core/services/movie.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-home-details',
  templateUrl: './home-details.page.html',
  styleUrls: ['./home-details.page.scss'],
})
export class HomeDetailsPage implements OnInit {
  card: CardModel = {} as CardModel;
  id: number;
  constructor(
    private movieService: MovieService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    
    this.movieService
      .getMovie(this.id)
      .then(movie => {
        console.log("movie: ", movie);
        this.card = movie;
      });
  }

}
