import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/core/services/api.service';
import { MovieModel } from 'src/app/core/model/movie-model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.page.html',
  styleUrls: ['./movies.page.scss'],
})
export class MoviesPage implements OnInit {
  movies: MovieModel[];
  constructor(
    private apiService: ApiService,
    private router: Router
  ) { }

  ngOnInit() {
    this.apiService.getPopularMovies().then(data => this.movies = data.results);
  }

  goToDetails(id: number) {
    this.router.navigate(['movies', id]);
  }

}
