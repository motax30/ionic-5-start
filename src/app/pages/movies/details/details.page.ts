import { Component, OnInit } from '@angular/core';
import { MovieModel } from 'src/app/core/model/movie-model';
import { ApiService } from 'src/app/core/services/api.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
})
export class DetailsPage implements OnInit {
  id: number;
  movieModel: MovieModel;

  constructor(
    private apiService: ApiService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.apiService.getMovieDetails(this.id).then(data => this.movieModel = data);
  }

}
