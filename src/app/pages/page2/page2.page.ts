import { Component, OnInit } from '@angular/core';
import { CardModel } from '../../core/model/card-model';

@Component({
  selector: 'app-page2',
  templateUrl: './page2.page.html',
  styleUrls: ['./page2.page.scss'],
})
export class Page2Page implements OnInit {
  pageTitle:string = "Meu Perfil";
  card1: CardModel = {
    id: 2,
    title: "Madison, WI",
    subtitle: "Destination",
    image: "https://blog.emania.com.br/wp-content/uploads/2017/02/2-2.jpg",
    content: "Founded in <strong>1829</strong> on an isthmus between Lake Monona and Lake Mendota, Madison was named the capital of the Wisconsin Territory in 1836."
  };
  card2: CardModel = {
    id: 1,
    title: "Madison, WI Card 2",
    subtitle: "Destination Card 2",
    image: "https://blog.emania.com.br/wp-content/uploads/2017/02/2-2.jpg",
    content: "Founded in <strong>1829</strong> on an isthmus between Lake Monona and Lake Mendota, Madison was named the capital of the Wisconsin Territory in 1836."
  };

  cards: Array<CardModel> = [];

  isAdmin = false;
  
  constructor() { }

  ngOnInit() {
    this.cards = [];
    this.cards.push(this.card1);
    this.cards.push(this.card2);
  }

  cardClicked(card: CardModel) {
    alert(card.title);
  }

}
